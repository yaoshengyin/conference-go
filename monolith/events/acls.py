import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_coordinates(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    data = response.json()
    try:
        lat = data[0]['lat']
        lon = data[0]['lon']
    except (KeyError, IndexError):
        lat, lon = None, None
    return {'lat': lat, 'lon': lon}


def get_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    response = requests.get(url, headers=headers)
    # data = json.loads(response.text)
    data = response.json()
    try:
        picture_url = data['photos'][0]['src']['medium']
    except (KeyError, IndexError):
        picture_url = None
    return {'picture_url': picture_url}


def get_weather_data(city, state):
    coordinates = get_coordinates(city, state)
    lat, lon = coordinates.get('lat', None), coordinates.get('lon', None)
    if lat and lon:
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
        response = requests.get(url)
        data = response.json()
        try:
            weather = {
                'temp': data['main']['temp'],
                'description': data['weather'][0]['description']
            }
        except KeyError:
            weather = None
        return weather
    else:
        return None

