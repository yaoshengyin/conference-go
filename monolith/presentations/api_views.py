import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": list(presentations.values())})

    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.create(**content)
            return JsonResponse({"id": presentation.id}, status=201)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    try:
        presentation = Presentation.objects.get(id=id)
    except Presentation.DoesNotExist:
        return JsonResponse({"error": "Presentation not found"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
            )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body.decode('utf-8'))
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
            )
