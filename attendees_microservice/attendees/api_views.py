import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO
from django.views.decorators.http import require_http_methods


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name"]

    def get_extra_data(self, o):
        return {
            "conference": {
                "name": o.conference.name,
                "href": o.conference.get_api_url(),
            }
        }


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]

    def get_extra_data(self, o):
        conference_data = {
            "name": o.conference.name,
            "href": o.conference.get_api_url()
        }
        return {"conference": conference_data}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse({"error": "Attendee not found"}, status=404)

    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False
    )